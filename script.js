var tabla,fila,div,texto;
    function AgregarInicio(){
    tabla=document.getElementById('tabla1');
    fila=document.createElement('tr');//se crea
    tabla.appendChild(fila);
    div=document.createElement('div');//se crea
    div.innerHTML = '<svg width="210" height="100">'+
    '<ellipse cx="100" cy="23" rx="50" ry="20" class="figura" />'+
    '<text x="80" y="25" font-family="Verdana" font-size="15">Inicio</text>'+
    '<rect x="100" y="45"  width="5" height="50" class="flecha"/>'+
    '<polygon points="92 90,112 90,102 100" class="flecha"/>'+
    '</svg>';
    fila.appendChild(div);

    textContent = document.getElementById('codigo');
    espacio=document.createElement('div');//se crea
    textContent.appendChild(espacio);
    espacio.innerHTML = '<p>int main(){ <br />';
}

function AgregarFinal(){
    tabla=document.getElementById('tabla1');
    fila=document.createElement('tr');//se crea
    tabla.appendChild(fila);
    div=document.createElement('div');//se crea
    div.innerHTML = '<svg width="210" height="45">'+
    '<ellipse cx="100" cy="23" rx="50" ry="20" class="figura" />'+
    '<text x="80" y="25" class="textos">FIN</text>'+
    '</svg>';
    fila.appendChild(div);

    textContent = document.getElementById('codigo');
    espacio=document.createElement('div');//se crea
    textContent.appendChild(espacio);
    espacio.innerHTML = '<p>} <br />';
}

function AgregarSentenciaSimple(){
    tabla=document.getElementById('tabla1');
    fila=document.createElement('tr');//se crea
    tabla.appendChild(fila);
    div=document.createElement('div');//se crea
    texto = prompt('Ingresa tu entrada de dato:')
    div.innerHTML = '<svg width="210" height="110">'+
    '<rect x="50" y="2" width="100" height="50" class="figura"/>'+
    '<rect x="100" y="55"  width="5" height="50" class="flecha"/>'+
    '<polygon points="92 100,112 100,102 110" class="flecha"/>'+
    '<text x="80" y="30" class="textos">'+texto+'</text>'+
    '</svg>';
    fila.appendChild(div);

    textContent = document.getElementById('codigo');
    espacio=document.createElement('div');//se crea
    textContent.appendChild(espacio);
    espacio.innerHTML = '<p>&nbsp;&nbsp;'+texto+';<br />';
}

function AgregarEntradaDatos(){
    tabla=document.getElementById('tabla1');
    fila=document.createElement('tr');//se crea
    tabla.appendChild(fila);
    div=document.createElement('div');//se crea
    texto = prompt('Ingresa tu entrada:')
    div.innerHTML = '<svg width="210" height="120">'+
    '<polygon points="50 0,50 60,75 70,95 70,105 60,125 50,135 50,160 60,160 0" class="figura"/>'+
    '<rect x="105" y="60"  width="5" height="50" class="flecha"/>'+
    '<polygon points="97 105,117 105,107 120" class="flecha"/>'+
    '<text x="70"  y="30" class="textos">'+texto+'</text>'+
    '</svg>';
    fila.appendChild(div);

    textContent = document.getElementById('codigo');
    espacio=document.createElement('div');//se crea
    textContent.appendChild(espacio);
    espacio.innerHTML = '<p>&nbsp;&nbsp;printf("'+texto+'");<br />';
}

function AgregarSalidaDatos(){
    tabla=document.getElementById('tabla1');
    fila=document.createElement('tr');//se crea
    tabla.appendChild(fila);
    div=document.createElement('div');//se crea
    texto = prompt('Ingresa tu salida :')
    div.innerHTML = '<svg width="210" height="120">'+
    '<polygon points="60 3,50 13,50 63,160 63,160 3" class="figura"/>'+
    '<rect x="105" y="65"  width="5" height="50" class="flecha"/>'+
    '<polygon points="97 105,117 105,107 120" class="flecha"/>'+
    '<text x="70"  y="40" class="textos">'+texto+'</text>'+
    '</svg>';
    fila.appendChild(div);

    textContent = document.getElementById('codigo');
    espacio=document.createElement('div');//se crea
    textContent.appendChild(espacio);
    espacio.innerHTML = '<p>&nbsp;&nbsp;scanf("%d",&'+texto+');<br />';
}

function AgregarDeclaracionVariables() {
    tabla=document.getElementById('tabla1');
    fila=document.createElement('tr');//se crea
    tabla.appendChild(fila);
    div=document.createElement('div');//se crea
    texto = prompt('Ingresa tu declaracion de variable:')
    div.innerHTML = '<svg width="210" height="120">'+
    '<rect x="50" y="0"  width="110" height="60" class="figura"/>'+
    '<rect x="50" y="0"  width="110" height="10" class="figura"/>'+
    '<rect x="50" y="50"  width="110" height="10" class="figura"/>'+
    '<rect x="110" y="60"  width="5" height="50" class="flecha"/>'+
    '<polygon points="102 105,122 105,112 120" class="flecha"/>'+
    '<text x="65"  y="35" class="textos">'+texto+'</text>'+
    '</svg>';
    fila.appendChild(div);

    textContent = document.getElementById('codigo');
    espacio=document.createElement('div');//se crea
    textContent.appendChild(espacio);
    espacio.innerHTML = '<p>&nbsp;&nbsp;int &nbsp;'+texto+';<br />';
}

function AgregarIF() {
    tabla=document.getElementById('tabla1');
    fila=document.createElement('tr');//se crea
    tabla.appendChild(fila);
    div=document.createElement('div');//se crea
    condicion = prompt('Ingresa tu condición:')
    instruccion1 = prompt('Ingresa tu instruccion en caso de no cumplir:')
    instruccion2 = prompt('Ingresa tu instruccion en caso de si cumplir:')
    div.innerHTML='<svg width="315" height="250">'+
    '<polygon points="65 30, 110 60, 160 30, 110 2" class="figuraIF"/>'+
    '<text x="80"  y="35" class="textos">'+condicion+'</text>'+
    '<text x="160"  y="25" >NO</text>'+
    '<text x="120"  y="70" >SI</text>'+
    '<rect x="50" y="125"  width="110" height="60" class="figura"/>'+
    '<text x="60"  y="155" class="textos">'+instruccion2+'</text>'+
    '<rect x="200" y="125"  width="110" height="60" class="figura"/>'+
    '<text x="210"  y="155" class="textos">'+instruccion1+'</text>'+
    '<rect x="110" y="60"  width="5" height="50" class="contornoIF"/>'+
    '<polygon points="102 105,122 105,112 120" class="contornoIF"/>'+
    '<rect x="160" y="28"  width="90" height="5" class="contornoIF"/>'+
    '<rect x="250" y="28"  width="5" height="88" class="contornoIF"/>'+
    '<polygon points="242 105,262 105,252 120" class="contornoIF"/>'+
    '<rect x="110" y="186"  width="5" height="50" class="flecha"/>'+
    '<polygon points="102 235,122 235,112 250" class="flecha"/>'+
    '<rect x="250" y="186"  width="5" height="30" class="flecha"/>'+
    '<rect x="110" y="215"  width="145" height="5" class="flecha"/>'+
    '</svg>';
    fila.appendChild(div);

    textContent = document.getElementById('codigo');
    espacio=document.createElement('div');//se crea
    textContent.appendChild(espacio);
    espacio.innerHTML = '<p>&nbsp;&nbsp;if ('+condicion+'){<br />'+
                        '<p>&nbsp;&nbsp;&nbsp;'+instruccion2+';'+
                        '<p>&nbsp;&nbsp;}else{'+
                        '<p>&nbsp;&nbsp;&nbsp;'+instruccion1+';'+
                        '<p>&nbsp;&nbsp;}';
}

function AgregarFor() {
    tabla=document.getElementById('tabla1');
    fila=document.createElement('tr');//se crea
    tabla.appendChild(fila);
    inicializacion = prompt('Ingresa tu inicialización:')
    condicion = prompt('Ingresa tu condicion:')
    incremento = prompt('Ingresa tu incremento o decremento:')
    texto1 = prompt('Ingresa tu instruccion para caso SI :')
    texto2 = prompt('Ingresa tu instruccion para caso NO :')
    div=document.createElement('div');//se crea
    div.innerHTML='<svg width="210" height="645">'+
    '<rect x="50" y="5"  width="110" height="60" class="figura"/>'+
    '<text x="60"  y="35" class="textos">'+inicializacion+'</text>'+
    '<rect x="100" y="65"  width="5" height="50" class="flecha"/>'+
    '<polygon points="92 110,112 110,102 120" class="flecha"/>'+

    '<polygon points="55 150, 100 180, 150 150, 100 120" class="figuraIF"/>'+
    '<text x="75"  y="155" class="textos">'+condicion+'</text>'+
    '<rect x="100" y="180"  width="5" height="50" class="contornoIF"/>'+
    '<polygon points="92 230,112 230,102 240" class="contornoIF"/>'+
    '<rect x="150" y="147"  width="30" height="5" class="contornoIF"/>'+
    '<rect x="180" y="147"  width="5" height="330" class="contornoIF"/>'+
    '<rect x="100" y="477"  width="85" height="5" class="contornoIF"/>'+
    '<rect x="100" y="477"  width="5" height="35" class="contornoIF"/>'+
    '<polygon points="92 512,112 512,102 522" class="contornoIF"/>'+
    '<text x="160"  y="140" >NO</text>'+
    '<text x="110"  y="190" >SI</text>'+

    '<rect x="50" y="525"  width="110" height="60" class="figura"/>'+
    '<text x="60"  y="555" class="textos">'+texto2+'</text>'+
    '<rect x="100" y="585"  width="5" height="50" class="flecha"/>'+
    '<polygon points="92 635,112 635,102 645" class="flecha"/>'+

    '<rect x="50" y="245"  width="110" height="60" class="figura"/>'+
    '<text x="60"  y="270" class="textos">'+texto1+'</text>'+
    '<rect x="100" y="305"  width="5" height="50" class="flecha"/>'+
    '<polygon points="92 355,112 355,102 365" class="flecha"/>'+

    '<rect x="50" y="370"  width="110" height="60" class="figura"/>'+
    '<text x="60"  y="400" class="textos">'+incremento+'</text>'+
    '<rect x="100" y="430"  width="5" height="30" class="flecha"/>'+
    '<rect x="30" y="460"  width="75" height="5" class="flecha"/>'+
    '<rect x="25" y="90"  width="5" height="375" class="flecha"/>'+
    '<rect x="30" y="90"  width="65" height="5" class="flecha"/>'+
    '<polygon points="90 82,90 102,100 92" class="flecha"/>'+
    '</svg>';
    fila.appendChild(div);

    textContent = document.getElementById('codigo');
    espacio=document.createElement('div');//se crea
    textContent.appendChild(espacio);
    espacio.innerHTML = '<p>&nbsp;&nbsp;for('+inicializacion+';'+condicion+';'+incremento+'){<br />'+
                        '<p>&nbsp;&nbsp;&nbsp;'+texto1+';'+
                        '<p>&nbsp;&nbsp;}'+
                        '<p>&nbsp;&nbsp;'+texto2+';';
}

function AgregarWhile() {
    tabla=document.getElementById('tabla1');
    fila=document.createElement('tr');//se crea
    tabla.appendChild(fila);
    condicion = prompt('Ingresa tu condicion:')
    texto1 = prompt('Ingresa tu instruccion 1 para caso SI :')
    texto2 = prompt('Ingresa tu instruccion 2 para caso SI :')
    texto3 = prompt('Ingresa tu instruccion para caso NO :')
    div=document.createElement('div');//se crea
    div.innerHTML= '<svg width="210" height="585">'+
    '<rect x="100" y="0"  width="5" height="50" class="flecha"/>'+
    '<polygon points="92 45,112 45,102 55" class="flecha"/>'+
    '<polygon points="55 85, 100 115, 150 85, 100 55" class="figuraIF"/>'+
    '<text x="75"  y="90" class="textos">'+condicion+'</text>'+
    '<rect x="100" y="115"  width="5" height="50" class="contornoIF"/>'+
    '<polygon points="92 165,112 165,102 175" class="contornoIF"/>'+
    '<rect x="150" y="82"  width="30" height="5" class="contornoIF"/>'+
    '<rect x="180" y="82"  width="5" height="330" class="contornoIF"/>'+
    '<rect x="100" y="412"  width="85" height="5" class="contornoIF"/>'+
    '<rect x="100" y="412"  width="5" height="35" class="contornoIF"/>'+
    '<polygon points="92 447,112 447,102 457" class="contornoIF"/>'+
    '<text x="160"  y="75" >NO</text>'+
    '<text x="110"  y="125" >SI</text>'+

    '<rect x="50" y="460"  width="110" height="60" class="figura"/>'+
    '<text x="60"  y="490" class="textos">'+texto3+'</text>'+
    '<rect x="100" y="520"  width="5" height="50" class="flecha"/>'+
    '<polygon points="92 570,112 570,102 585" class="flecha"/>'+

    '<rect x="50" y="180"  width="110" height="60" class="figura"/>'+
    '<text x="60"  y="205" class="textos">'+texto1+'</text>'+
    '<rect x="100" y="240"  width="5" height="50" class="flecha"/>'+
    '<polygon points="92 290,112 290,102 300" class="flecha"/>'+

    '<rect x="50" y="305"  width="110" height="60" class="figura"/>'+
    '<text x="60"  y="335" class="textos">'+texto1+'</text>'+
    '<rect x="100" y="365"  width="5" height="30" class="flecha"/>'+
    '<rect x="30" y="395"  width="75" height="5" class="flecha"/>'+
    '<rect x="25" y="25"  width="5" height="375" class="flecha"/>'+
    '<rect x="30" y="25"  width="65" height="5" class="flecha"/>'+
    '<polygon points="90 17,90 37,100 27" class="flecha"/>'+
    '</svg>';
    fila.appendChild(div);

    textContent = document.getElementById('codigo');
    espacio=document.createElement('div');//se crea
    textContent.appendChild(espacio);
    espacio.innerHTML = '<p>&nbsp;&nbsp;While('+condicion+'){<br />'+
                        '<p>&nbsp;&nbsp;&nbsp;'+texto1+';'+
                        '<p>&nbsp;&nbsp;&nbsp;'+texto2+';'+
                        '<p>&nbsp;&nbsp;}'+
                        '<p>&nbsp;&nbsp;'+texto3+';';
}

function AgregarDOWHILE() {
    tabla=document.getElementById('tabla1');
    fila=document.createElement('tr');//se crea
    tabla.appendChild(fila);
    texto1 = prompt('Ingresa tu instruccion 1 para caso SI :')
    texto2 = prompt('Ingresa tu instruccion 2 para caso SI :')
    condicion = prompt('Ingresa tu condicion:')
    texto3 = prompt('Ingresa tu instruccion para caso NO :')
    div=document.createElement('div');//se crea
    div.innerHTML= ' <svg width="210" height="545">'+
    '<rect x="100" y="0"  width="5" height="50" class="flecha"/>'+
    '<polygon points="92 45,112 45,102 55" class="flecha"/>'+
    '<polygon points="55 342, 100 372, 150 342, 100 310" class="figuraIF"/>'+
    '<text x="75"  y="348" class="textos">'+condicion+'</text>'+
    '<rect x="100" y="372"  width="5" height="35" class="contornoIF"/>'+
    '<polygon points="92 407,112 407,102 417" class="contornoIF"/>'+
    '<text x="120"  y="380" >NO</text>'+
    '<text x="40"  y="330" >SI</text>'+

    '<rect x="50" y="420"  width="110" height="60" class="figura"/>'+
    '<text x="60"  y="450" class="textos">'+texto3+'</text>'+
    '<rect x="100" y="480"  width="5" height="50" class="flecha"/>'+
    '<polygon points="92 530,112 530,102 545" class="flecha"/>'+

    '<rect x="50" y="60"  width="110" height="60" class="figura"/>'+
    '<text x="60"  y="95" class="textos">'+texto1+'</text>'+
    '<rect x="100" y="120"  width="5" height="50" class="flecha"/>'+
    '<polygon points="92 170,112 170,102 180" class="flecha"/>'+

    '<rect x="50" y="185"  width="110" height="60" class="figura"/>'+
    '<text x="60"  y="220" class="textos">'+texto2+'</text>'+
    '<rect x="100" y="245"  width="5" height="50" class="flecha"/>'+
    '<polygon points="92 295,112 295,102 305" class="flecha"/>'+

    '<rect x="30" y="340"  width="25" height="5" class="contornoIF"/>'+
    '<rect x="25" y="25"  width="5" height="320" class="contornoIF"/>'+
    '<rect x="30" y="25"  width="65" height="5" class="contornoIF"/>'+
    '<polygon points="90 17,90 37,100 27" class="contornoIF"/>'+
    '</svg>';
    fila.appendChild(div);

    textContent = document.getElementById('codigo');
    espacio=document.createElement('div');//se crea
    textContent.appendChild(espacio);
    espacio.innerHTML = '<p>&nbsp;&nbsp;do{'+
                        '<p>&nbsp;&nbsp;&nbsp;'+texto1+';'+
                        '<p>&nbsp;&nbsp;&nbsp;'+texto2+';'+
                        '<p>&nbsp;&nbsp;}while('+condicion+')<br />'+
                        '<p>&nbsp;&nbsp;'+texto3+';';
}

function cambiarContorno() {
    C = document.getElementById('color').value;
    var color = document.querySelectorAll(".figura");
    for ( var i=0; i<color.length; i++){
        color[i].style.stroke = C;
    }
}
function cambiarRelleno() {
    C = document.getElementById('relleno').value;
    var color = document.querySelectorAll(".figura");
    for ( var i=0; i<color.length; i++){
        color[i].style.fill = C;
    }
}
function cambiarFlecha() {
    C = document.getElementById('flecha').value;
    var color = document.querySelectorAll(".flecha");
    for ( var i=0; i<color.length; i++){
        color[i].style.fill = C;
    }
}
function cambiarFuente(n) {
    var texto = document.querySelectorAll(".textos");
    if (n==1){
        for ( var i=0; i<texto.length; i++){
            texto[i].style.font = '15px cursive';
        }
    }
    if (n==2){
        for ( var i=0; i<texto.length; i++){
            texto[i].style.font = '10px Courier';
        }
    }
    if (n==3){
        for ( var i=0; i<texto.length; i++){
            texto[i].style.font = '15px sans-serif';
        }
    }
    if (n==4){
        for ( var i=0; i<texto.length; i++){
            texto[i].style.font = '15px fantasy';
        }
    }
    if (n==5){
        for ( var i=0; i<texto.length; i++){
            texto[i].style.font = '15px garamond';
        }
    }
}
function EliminarFinal(){
    if(tabla.hasChildNodes()) //saber si tiene nodos hijos
        tabla.removeChild(tabla.lastChild); //eliminar el nodo hijo
    if(textContent.hasChildNodes())
        textContent.removeChild(textContent.lastChild)
}
